﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tilemap : MonoBehaviour
{
	public Renderer TilemapRenderer;

    // Start is called before the first frame update
    void Start()
    {
		TilemapRenderer.receiveShadows = true;
		TilemapRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
