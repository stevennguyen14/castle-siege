﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
	//public
	public Transform nozzle;
	public GameObject muzzleFlash;
	public float turretRange;
	public float fireRate;
	public AudioClip shootSound;

	//private 
	private float newDistance;
	private float nextFire;
	private AudioSource audioSource;

	void Start()
	{
		audioSource = GameManager.instance.GetComponent<AudioSource>();
	}

	void FixedUpdate()
	{
		LookAtEnemy();
	}


	public void IncreaseFireRate()
	{
		//make sure firerate can't go negative
		if(fireRate >= 0)
		{
			fireRate -= 0.05f;
		}
	}

	void LookAtEnemy()
	{
		foreach (Enemy enemy in GameManager.instance.enemyArray)
		{
			//check if active first
			if (enemy.gameObject.activeSelf)
			{
				//look at closest enemy
				float distance = Vector3.Distance(enemy.gameObject.transform.position, transform.position);
				//if distance is close enough then look at the enemy
				if (distance < turretRange)
				{
					transform.LookAt(new Vector3(enemy.gameObject.transform.position.x, transform.position.y, enemy.gameObject.transform.position.z));
					nextFire += Time.deltaTime;
					if (nextFire > fireRate)
					{
						nextFire = 0;
						Shoot();
					}
				}
			}
		}

		foreach (Enemy enemy in GameManager.instance.enemyArray2)
		{
			//check if active first
			if (enemy.gameObject.activeSelf)
			{
				//look at closest enemy
				float distance = Vector3.Distance(enemy.gameObject.transform.position, transform.position);
				//if distance is close enough then look at the enemy
				if (distance < turretRange)
				{
					transform.LookAt(new Vector3(enemy.gameObject.transform.position.x, transform.position.y, enemy.gameObject.transform.position.z));
					nextFire += Time.deltaTime;
					if (nextFire > fireRate)
					{
						nextFire = 0;
						Shoot();
					}
				}
			}

		}

		foreach (Enemy enemy in GameManager.instance.enemyArray3)
		{
			//check if active first
			if (enemy.gameObject.activeSelf)
			{
				//look at closest enemy
				float distance = Vector3.Distance(enemy.gameObject.transform.position, transform.position);
				//if distance is close enough then look at the enemy
				if (distance < turretRange)
				{
					transform.LookAt(new Vector3(enemy.gameObject.transform.position.x, transform.position.y, enemy.gameObject.transform.position.z));
					nextFire += Time.deltaTime;
					if (nextFire > fireRate)
					{
						nextFire = 0;
						Shoot();
					}
				}
			}

		}

	}

	void Shoot()
	{
		//if this turret is a gatling turret then use normal bullets
		if (this.gameObject.tag == "Gatling")
		{
			//creating out own instantiation from bulletmanager instance
			Bullet bullet = GameManager.instance.Instantiate();

			//set position and rotation of bullet to the nozzle of tower
			bullet.transform.position = nozzle.position;
			bullet.transform.rotation = nozzle.parent.rotation;
			//set the bullet active, the bullet will deactivate within the bullet script after a few seconds
			bullet.gameObject.SetActive(true);

		}
		//if this turret is a rocket, then use rockets
		if (this.gameObject.tag == "Rocket")
		{
			//creating out own instantiation from bulletmanager instance
			Rocket rocket = GameManager.instance.InstantiateRocket();

			//set position and rotation of bullet to the nozzle of tower
			rocket.transform.position = nozzle.position;
			rocket.transform.rotation = nozzle.parent.rotation;
			//set the bullet active, the bullet will deactivate within the bullet script after a few seconds
			rocket.gameObject.SetActive(true);
		}

		audioSource.PlayOneShot(shootSound);

		StartCoroutine("MuzzleFlash");
	}

	IEnumerator MuzzleFlash()
	{
		muzzleFlash.SetActive(true);
		yield return new WaitForSeconds(0.05f);
		muzzleFlash.SetActive(false);
	}
}
