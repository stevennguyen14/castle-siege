﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

	public GameObject tutorial;
	   
	public void StartGame()
	{
		SceneManager.LoadScene("loading");
	}

	public void Tutorial()
	{
		tutorial.SetActive(true);
	}

	public void BackButton()
	{
		tutorial.SetActive(false);
	}

	public void Quit()
	{
		Application.Quit();
	}

}
