﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GM : MonoBehaviour
{

	public string[] script;
	int scriptIndex;
	public Text dialogueText;

	[System.Serializable]
	public class SuperScript
	{
		public string[] script;
	}

	public SuperScript[] superScript;
	int superScriptIndex;

    // Start is called before the first frame update
    void Start()
    {
		superScriptIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetMouseButtonDown(0))
		{
			ShowNextScript();
		}
    }

	void ShowNextScript()
	{
		if (scriptIndex >= superScript[superScriptIndex].script.Length)
		{
			scriptIndex = 0;
			superScriptIndex++;
		}
		StopCoroutine("PlayText");
		dialogueText.text = "";

		StartCoroutine("PlayText");
		scriptIndex++;
	}

	IEnumerator PlayText()
	{
		foreach (char c in superScript[superScriptIndex].script[scriptIndex])
		{
			dialogueText.text += c;
			yield return new WaitForSeconds(0.03f);
		}
	}


}
