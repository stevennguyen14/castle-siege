﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{

	public float explosionRadius;
	public GameObject explosionEffect;
	public AudioClip explodeSound;


	private AudioSource audioSource;

    // Start is called before the first frame update
    void OnEnable()
	{
		StartCoroutine(Flying());
		audioSource = Camera.main.GetComponent<AudioSource>();
	}

	void Start()
	{
		
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	void OnTriggerEnter(Collider col)
	{
		print("collided with" + col.gameObject.name);

		Destroy(Instantiate(explosionEffect, transform.position, transform.rotation), 1f);

		audioSource.PlayOneShot(explodeSound);

		Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);

		foreach(Collider nearbyObject in colliders)
		{
			if (nearbyObject.gameObject.CompareTag("Enemy1"))
			{
				nearbyObject.gameObject.GetComponent<Enemy>().DeductEnemy1HP();
				//print("health: " + col.gameObject.GetComponent<Enemy>().GetRedHP());
				if (nearbyObject.gameObject.GetComponent<Enemy>().GetEnemy1HP() <= 0)
				{
					nearbyObject.gameObject.SetActive(false);
					GameManager.instance.AddPoints(20);
				}
			}
			if (nearbyObject.gameObject.CompareTag("Enemy2"))
			{
				nearbyObject.gameObject.GetComponent<Enemy>().DeductEnemy2HP();
				//print("health: " + col.gameObject.GetComponent<Enemy>().GetBlueHP());
				if (nearbyObject.gameObject.GetComponent<Enemy>().GetEnemy2HP() <= 0)
				{
					nearbyObject.gameObject.SetActive(false);
					GameManager.instance.AddPoints(30);
				}
			}
			if (nearbyObject.gameObject.CompareTag("Enemy3"))
			{
				nearbyObject.gameObject.GetComponent<Enemy>().DeductEnemy3HP();
				//print("health: " + col.gameObject.GetComponent<Enemy>().GetYellowHP());
				if (nearbyObject.gameObject.GetComponent<Enemy>().GetEnemy3HP() <= 0)
				{
					nearbyObject.gameObject.SetActive(false);
					GameManager.instance.AddPoints(40);
				}
			}
		}

		//check if rocket penetration buff is enabled
		if (!GameManager.instance.GetRocketPenetration())
		{
			gameObject.SetActive(false);
		}
		

	}

	IEnumerator Flying()
	{

		//moving the bullet forwards
		for (float t = 0; t < 2f; t += Time.deltaTime)
		{
			transform.Translate(0, 0, 25f * Time.deltaTime);
			yield return null;
		}

		//deactivate bullet after 2 seconds
		gameObject.SetActive(false);
	}
}
