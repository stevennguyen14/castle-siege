﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	//public
	public static GameManager instance;
	public float enemySpeed;
	public int enemy1HP, enemy2HP, enemy3HP;
	public Text scoreText, waveText;
	public int health;
	public Slider healthSlider;
	public GameObject gameOverUI, startGameUI, pauseUI, tutorialUI, terrain;
	public GameObject fire1, fire2, fire3;
	public GameObject gatling, rocketLauncher;

	//private
	private int points;
	private bool rocketPenetration = false;
	private bool paused;

	void Awake()
	{
		instance = this;
		gameOverUI.SetActive(false);
		pauseUI.SetActive(false);
	}

	//Unit Arrays
	public GameObject[] turrets;
	public Bullet[] bulletArray;
	public Rocket[] rocketArray;
	public Enemy[] enemyArray, enemyArray2, enemyArray3;
	public Transform[] wayPoints;
	public string[] manyWaves;
	public Transform spawn;
	int bulletIndex;

	void Start()
	{
		health = 100;
		healthSlider.value = health;
		points = 500;
		scoreText.text = ": " + points;
	}

	public void StartGame()
	{
		StartCoroutine("SpawnWave");
		startGameUI.SetActive(false);
	}

	public void RestartGame()
	{
		SceneManager.LoadScene("main");
	}

	public void HowToPlay()
	{
		pauseUI.SetActive(false);
		tutorialUI.SetActive(true);

	}

	public void BackButton()
	{
		pauseUI.SetActive(true);
		tutorialUI.SetActive(false);
	}

	public void PauseButton()
	{
		if (!paused)
		{
			Time.timeScale = 0;
			pauseUI.SetActive(true);
			paused = true;
		}
		else
		{
			Time.timeScale = 1;
			pauseUI.SetActive(false);
			paused = false;
		}

	}

	public void QuitGame()
	{
		Application.Quit();
	}

	public void MainMenu()
	{
		SceneManager.LoadScene("mainMenu");
		Time.timeScale = 1;
	}

	public void DeductHealth()
	{
		health -= 10;
		healthSlider.value = health;
		if(health <= 75)
		{
			fire1.SetActive(true);
		}
		if (health <= 50)
		{
			fire2.SetActive(true);
		}
		if (health <= 25)
		{
			fire3.SetActive(true);
		}
		if (health <= 0)
		{
			gameOverUI.SetActive(true);
		}
	}

	public void AddPoints(int points)
	{
		this.points += points;
		scoreText.text = ": " + this.points;
	}

	public void AddGatling()
	{
		//make sure enough points
		if(points >= 300)
		{
			//enable the floorObjectPlacement script within the terrain
			terrain.SetActive(true);
			terrain.GetComponent<floorObjectPlacement>().SetPlacementObject(gatling);
			points -= 300;
			scoreText.text = ": " + points;
		}
	}

	public void AddRocketLauncher()
	{
		//make sure enough points
		if (points >= 1200)
		{
			//enable the floorObjectPlacement script within the terrain
			terrain.SetActive(true);
			terrain.GetComponent<floorObjectPlacement>().SetPlacementObject(rocketLauncher);
			points -= 1200;
			scoreText.text = ": " + points;
		}
	}

	public void IncreaseFireRate()
	{
		if(points >= 700)
		{
			turrets = GameObject.FindGameObjectsWithTag("Gatling");
			if(turrets != null)
			{
				foreach (GameObject turret in turrets)
				{
					turret.GetComponent<Turret>().IncreaseFireRate();
				}
				points -= 700;
				scoreText.text = ": " + points;
			}
		}
	}

	public void SetRocketPenetration()
	{
		if (points >= 1000 && !rocketPenetration)
		{
			points -= 1000;
			scoreText.text = ": " + points;
			rocketPenetration = true;
		}
		
	}

	public bool GetRocketPenetration()
	{
		return rocketPenetration;
	}

	//creating our own instantiation function
	public Bullet Instantiate()
	{
		foreach (Bullet baby in bulletArray)
		{
			if (!baby.gameObject.activeSelf)
			{
				return baby;
			}
		}

		return null;
	}

	public Rocket InstantiateRocket()
	{
		foreach (Rocket baby in rocketArray)
		{
			if (!baby.gameObject.activeSelf)
			{
				return baby;
			}
		}

		return null;
	}

	Enemy InstantiateEnemy(int whichOne)
	{
		switch (whichOne)
		{
			case 0:
				foreach (Enemy baby in enemyArray)
				{
					if (!baby.gameObject.activeSelf)
					{
						return baby;
					}
				}
				break;
			case 1:
				foreach (Enemy baby in enemyArray2)
				{
					if (!baby.gameObject.activeSelf)
					{
						return baby;
					}
				}
				break;
			case 2:
				foreach (Enemy baby in enemyArray3)
				{
					if (!baby.gameObject.activeSelf)
					{
						return baby;
					}
				}
				break;
		}

		return null;
	}

	void Spawn(int enemyType)
	{
		//creating out own instantiation from bulletmanager instance
		Enemy baby = InstantiateEnemy(enemyType);

		//set position and rotation of bullet to the nozzle of tower
		baby.transform.position = spawn.position;
		baby.transform.rotation = spawn.rotation;
		//set the enemy active, the enemy will deactivate within the enemy script after a few seconds
		baby.gameObject.SetActive(true);
	}

	[System.Serializable]
	public class wavesData
	{
		public int enemy1;
		public int enemy2;
		public int enemy3;
		public int enemy1Health, enemy2Health, enemy3Health;

		public float spawnRate;
		public float duration;
		public float speed;
	}

	public wavesData[] wavesDESIGN;

	void AssignWaveStats(int index)
	{
		enemy1HP = wavesDESIGN[index].enemy1Health;
		enemy2HP = wavesDESIGN[index].enemy2Health;
		enemy3HP = wavesDESIGN[index].enemy3Health;
		enemySpeed = wavesDESIGN[index].speed;
	}

	public int getEnemy1Health()
	{
		return enemy1HP;
	}
	public int getEnemy2Health()
	{
		return enemy2HP;
	}
	public int getEnemy3Health()
	{
		return enemy3HP;
	}
	public float getSpeed()
	{
		return enemySpeed;
	}


	IEnumerator SpawnWave()
	{
		for(int i = 0; i < wavesDESIGN.Length; i++)
		{
			print("WAVE " + (i + 1));
			waveText.text = "WAVE " + (i + 1);
			AssignWaveStats(i);
			//assign the wave health so it can be access by other scripts
			for (int a = 0; a < wavesDESIGN[i].enemy1; a++)
			{
				Spawn(0);
				yield return new WaitForSeconds(wavesDESIGN[i].spawnRate * Time.deltaTime);
			}

			for (int a = 0; a < wavesDESIGN[i].enemy2; a++)
			{
				Spawn(1);
				yield return new WaitForSeconds(wavesDESIGN[i].spawnRate * Time.deltaTime);
			}

			for (int a = 0; a < wavesDESIGN[i].enemy3; a++)
			{
				Spawn(2);
				yield return new WaitForSeconds(wavesDESIGN[i].spawnRate * Time.deltaTime);
			}
			yield return new WaitForSeconds(wavesDESIGN[i].duration);
		}
	}
}
