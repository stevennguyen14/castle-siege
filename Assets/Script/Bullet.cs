﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    // Start is called before the first frame update
    void OnEnable()
    {
		StartCoroutine(Flying());
    }

    // Update is called once per frame
    void Update()
    {
		
    }

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.CompareTag("Enemy1"))
		{
			col.gameObject.GetComponent<Enemy>().DeductEnemy1HP();
			//print("health: " + col.gameObject.GetComponent<Enemy>().GetRedHP());
			if(col.gameObject.GetComponent<Enemy>().GetEnemy1HP() <= 0)
			{
				col.gameObject.SetActive(false);
				GameManager.instance.AddPoints(20);
			}
		}
		if (col.gameObject.CompareTag("Enemy2"))
		{
			col.gameObject.GetComponent<Enemy>().DeductEnemy2HP();
			//print("health: " + col.gameObject.GetComponent<Enemy>().GetBlueHP());
			if (col.gameObject.GetComponent<Enemy>().GetEnemy2HP() <= 0)
			{
				col.gameObject.SetActive(false);
				GameManager.instance.AddPoints(30);
			}
		}
		if (col.gameObject.CompareTag("Enemy3"))
		{
			col.gameObject.GetComponent<Enemy>().DeductEnemy3HP();
			//print("health: " + col.gameObject.GetComponent<Enemy>().GetYellowHP());
			if (col.gameObject.GetComponent<Enemy>().GetEnemy3HP() <= 0)
			{
				col.gameObject.SetActive(false);
				GameManager.instance.AddPoints(40);
			}
		}

	}

	IEnumerator Flying()
	{

		//moving the bullet forwards
		for(float t = 0; t < 2f; t += Time.deltaTime)
		{
			transform.Translate(0, 0, 50f * Time.deltaTime);
			yield return null;
		}

		//deactivate bullet after 2 seconds
		gameObject.SetActive(false);
	}
}
