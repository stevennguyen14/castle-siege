﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{

	float mHori;
	public Transform head;

	public Transform nozzle;
	public Transform[] bullet;

	public Transform[] enemy;
	int enemyIndex;

	public Transform[] spawnPts;

	int bulletIndex;

    // Start is called before the first frame update
    IEnumerator Start()
    {
		while (true)
		{
			enemy[enemyIndex].position = spawnPts[Random.Range(0, spawnPts.Length)].position;
			enemy[enemyIndex].gameObject.SetActive(true);
			enemyIndex++;
			yield return new WaitForSeconds(1);
		}
    }

    // Update is called once per frame
    void Update()
    {
		mHori = Input.GetAxis("Mouse X");

		head.Rotate(0, mHori, 0);

		if (Input.GetMouseButtonDown(0))
		{
			Shoot();
		}
    }

	void Shoot()
	{
		bullet[bulletIndex].position = nozzle.position;
		bullet[bulletIndex].rotation = nozzle.rotation;
		bullet[bulletIndex].gameObject.SetActive(true);
		bullet[bulletIndex].GetComponent<Bullet>().StartCoroutine("Flying");
		bulletIndex++;
		if (bulletIndex >= bullet.Length) bulletIndex = 0;
	}
}
