﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	//public
	public int enemy1HP, enemy2HP, enemy3HP;

	//private
	Transform destination;

	void OnEnable()
	{
		UpdateStats();
		StartCoroutine("move");
	}

	// Start is called before the first frame update
	IEnumerator move()
    {

		foreach (Transform t in GameManager.instance.wayPoints)
		{
			destination = t;

			transform.LookAt(destination);

			while (distance() > 1f)
			{
				transform.Translate(0, 0, GameManager.instance.enemySpeed * Time.deltaTime);
				yield return null;
			}

		}

		gameObject.SetActive(false);

    }

	float distance()
	{
		return Vector3.Distance(this.transform.position, destination.position);
	}

	void UpdateStats()
	{
		enemy1HP = GameManager.instance.getEnemy1Health();
		enemy2HP = GameManager.instance.getEnemy2Health();
		enemy3HP = GameManager.instance.getEnemy3Health();
	}

	public void DeductEnemy1HP()
	{
		enemy1HP--;
	}

	public int GetEnemy1HP()
	{
		return enemy1HP;
	}

	public void DeductEnemy2HP()
	{
		enemy2HP--;
	}

	public int GetEnemy2HP()
	{
		return enemy2HP;
	}

	public void DeductEnemy3HP()
	{
		enemy3HP--;
	}

	public int GetEnemy3HP()
	{
		return enemy3HP;
	}
}
