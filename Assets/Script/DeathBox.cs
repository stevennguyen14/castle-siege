﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathBox : MonoBehaviour
{

	public GameObject explosion;
	public Transform explosionPosition;
	public AudioClip explodeSound;

	private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
		audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.CompareTag("Enemy1") || col.gameObject.CompareTag("Enemy2") || col.gameObject.CompareTag("Enemy3"))
		{
			GameManager.instance.DeductHealth();
			audioSource.PlayOneShot(explodeSound);
			Destroy(Instantiate(explosion, explosionPosition.position, explosionPosition.rotation), 2f);
			StartCoroutine(Shake(0.5f,0.5f));
		}
	}

	public IEnumerator Shake(float duration, float magnitude)
	{
		Vector3 orignalPosition = Camera.main.transform.position;
		float elapsed = 0f;

		while (elapsed < duration)
		{
			float x = Random.Range(-1f, 1f) * magnitude;
			float y = Random.Range(-1f, 1f) * magnitude;

			Camera.main.transform.position = (new Vector3(x, y, 0f)) + orignalPosition;
			elapsed += Time.deltaTime;
			yield return 0;
		}
		Camera.main.transform.position = orignalPosition;
	}
}
